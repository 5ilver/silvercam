#!/bin/bash


# This might be a useful piece of software archeology some day but
# in 2021 this script is basically useless. Use megapixels instead.
exit 1



function pic() {
#Thanks amom for helping test. Originally based on this:
#https://git.sr.ht/~martijnbraam/python-pinecamera     
cd ~
media-ctl -d /dev/media1 --set-v4l2 '"ov5640 3-004c":0[fmt:UYVY8_2X8/1280x720@1/20]' 
v4l2-ctl --device /dev/video1 --set-fmt-video="width=1280,height=720,pixelformat=UYVY"
ffplay -f v4l2 -framerate 20 -video_size 1280x720 -i /dev/video1 -exitonmousedown -vf "transpose=1" -x 640 -y 360 ;
newfilename=IMG_$(date +%Y%m%d_%H%M%S).jpg
ffmpeg -f v4l2 -framerate 20 -video_size 1280x720 -ss 00:00:00.5 -i /dev/video1 -frames:v 1 -vf "transpose=1" $newfilename ;
ffplay $newfilename -exitonmousedown -x 640 -y 360
}

function torch() {
if [ $(cat /sys/class/leds/white\:flash/brightness) == "0" ] ; 
then
echo 1 > /sys/class/leds/white\:flash/brightness ;
else
echo 0 > /sys/class/leds/white\:flash/brightness ;
fi
}

function install(){
apk add ffmpeg v4l-utils || pacman -Sy ffmpeg v4l-utils || apt-get -y install ffmpeg v4l-utils

cp $0 /usr/local/bin/silvercam.sh
chmod +x /usr/local/bin/silvercam.sh
(
cat <<'TEXT'
[Desktop Entry]
Name=Take a Picture
GenericName=Take a Picture
Comment=Take a Picture
Icon=preferences-desktop-display
Exec=/usr/local/bin/silvercam.sh pic
Type=Application
StartupNotify=true
Terminal=false
TEXT
)>/usr/share/applications/pic.desktop
(
cat <<'TEXT'
[Desktop Entry]
Name=Toggle Torch
GenericName=Toggle Torch
Comment=Toggle Torch
Icon=preferences-desktop-display
Exec=/usr/local/bin/silvercam.sh torch
Type=Application
StartupNotify=true
Terminal=false
TEXT
)>/usr/share/applications/torch.desktop
}

case $1 in
	torch )
		torch ;;
	pic )
		pic ;;
	install )
		install ;;
	* )
		echo "Use: $0 [torch|pic|install]"
		echo "Please run $0 install with sudo or as root"
esac

